﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GearManager : MonoBehaviour
{
	public List<GameObject> Gears;
	
	public float delayBetweenGearRotations = 0.1f;
	
	private RotateGear rotateGearScript;
	
	public GameObject LastArrow;
	
	private bool rotate = false;
	private bool locked = false;
	private bool GameWon = false;
	private bool GameOver = false;
	
	public GameObject BoxBase;
	
	public string NextLevelName;
	
	public GameObject WinGameScreen;
	public GameObject GameOverScreen;

	// Use this for initialization
	void Start ()
	{
			
		// The last arrow will be child of the last gear in chain so it moves with it.
		if(Gears.Count > 0)
		{
			GameObject copiaArrow = Instantiate(LastArrow) as GameObject;
			copiaArrow.transform.parent = Gears[Gears.Count-1].transform;
			copiaArrow.transform.localPosition = new Vector2(1,0);
			
			// Set the first direction random
			int first_direction = Random.Range(0,2);
			if( first_direction == 0 )
			{
				// Positive direction
				Gears[0].GetComponent<RotateGear>().SetDirection(1);
				foreach(Transform child in BoxBase.transform)
				{
					if(child.tag == "positive")
						child.gameObject.SetActive(true);
					if(child.tag == "negative")
						child.gameObject.SetActive(false);
				}
				
			}
			else
			{
				// Negative direction
				Gears[0].GetComponent<RotateGear>().SetDirection(-1);
				foreach(Transform child in BoxBase.transform)
				{
					if(child.tag == "positive")
						child.gameObject.SetActive(false);
					if(child.tag == "negative")
						child.gameObject.SetActive(true);
				}
			}
		}
	}
	
	
	/// <summary>
	/// Rotates the gears.
	/// </summary>
	/// <returns>The gears.</returns>
	IEnumerator RotateGears()
	{
		for(int i = 1; i < Gears.Count; i++)
		{
			yield return new WaitForSeconds(delayBetweenGearRotations);
			
			rotateGearScript = Gears[i].GetComponent<RotateGear>();
			
			rotateGearScript.SetDirection( -Gears[i-1].GetComponent<RotateGear>().GetDirection() );
			
			// speed[i] = diameter[i-1]*speed[i-1]/diameter[i]
			rotateGearScript.SetRotationSpeed( Gears[i-1].GetComponent<RotateGear>().GetSize() *
			                                  Gears[i-1].GetComponent<RotateGear>().GetRotationSpeed() /
			                                  rotateGearScript.GetSize() );
			                                  
			
			
			rotateGearScript.StartRotation();
		}
		
	}
	
	/// <summary>
	/// Stops the gears.
	/// </summary>
	/// <returns>The gears.</returns>
	IEnumerator StopGears()
	{
		// Lets stop them backwards! :)
		for(int i = Gears.Count -1; i >= 0; i--)
		{
			yield return new WaitForSeconds(delayBetweenGearRotations/Gears.Count);
			
			rotateGearScript = Gears[i].GetComponent<RotateGear>();
			
			rotateGearScript.StopRotation();
		}
		
	}
	
	/// <summary>
	/// Starts the rotating gears.
	/// </summary>
	public void StartRotatingGears()
	{
		if(Gears.Count > 0)
		{
			rotate = true;
		
			rotateGearScript = Gears[0].GetComponent<RotateGear>();
			rotateGearScript.StartRotation();
			
			StartCoroutine(RotateGears());
		}
	}
	
	/// <summary>
	/// Stops the rotating gears.
	/// </summary>
	public void StopRotatingGears()
	{
		rotate = false;
		
		StopCoroutine(RotateGears());
		
		StartCoroutine(StopGears());
	}
	
	public void LockOptions()
	{
		locked = true;
	}
	
	public bool AreOptionsLocked()
	{
		return locked;
	}
	
	public void WinGame()
	{
		GameWon = true;
	}
	
	public void LoseGame()
	{
		GameOver = true;
	}
	
	public IEnumerator GameIsWon()
	{
		yield return new WaitForSeconds(1);
		
		NGUITools.SetActive( WinGameScreen, true);
		
	}
	
	public IEnumerator GameIsLosed()
	{
		yield return new WaitForSeconds(1);
		
		NGUITools.SetActive( GameOverScreen, true);
		
		
	}
	
	public void LoadNextLevel()
	{
		if( NextLevelName.Length>0 )
			Application.LoadLevel( NextLevelName );
	}
	
	public void ReloadLevel()
	{
		if( NextLevelName.Length>0 )
			Application.LoadLevel( Application.loadedLevelName );
	}
	void FixedUpdate ()
	{
		if( GameWon)
			StartCoroutine(GameIsWon());
		if( GameOver)
			StartCoroutine(GameIsLosed());
	}
}