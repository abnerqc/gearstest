﻿using UnityEngine;
using System.Collections;

public class RotateGear : MonoBehaviour
{

	float size;
	public float direction = 1;
	public float rotationSpeed = 1f;
	
	private float angle;
	private bool rotate;
	

	// Use this for initialization
	void Start ()
	{
		size = renderer.bounds.size.x;
		angle = rotationSpeed;
		rotate = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(rotate)
		{
			transform.localEulerAngles = new Vector3(0,0, angle*direction*Time.fixedDeltaTime);
			
			angle += rotationSpeed;
		}
		else
		{
			
		}
	}
	
	/// <summary>
	/// Stops the rotation.
	/// </summary>
	public void StopRotation()
	{
		rotate = false;
	}
	
	/// <summary>
	/// Starts the rotating.
	/// </summary>
	public void StartRotation()
	{
		rotate = true;
	}
	
	public float GetDirection()
	{
		return direction;
	}
	
	public void SetDirection(float newDirection)
	{
		direction = newDirection;
	}
	
	public float GetRotationSpeed()
	{
		return rotationSpeed;
	}
	
	public void SetRotationSpeed(float newRotationSpeed)
	{
		rotationSpeed = newRotationSpeed;
	}
	
	public float GetSize()
	{
		return size;
	}
}
