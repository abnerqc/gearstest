﻿using UnityEngine;
using System.Collections;

public class SelectOption : MonoBehaviour 
{
	private GearManager gearManagerScript;
	
	private bool thisSelected = false;
	
	void Start()
	{
		gearManagerScript = GameObject.Find("GameManager").GetComponent<GearManager>();
	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) 
		{
			Ray ray = Camera.main.ScreenPointToRay( Input.GetTouch(0).position );
			RaycastHit hit;
			
			if ( Physics.Raycast(ray, out hit) && hit.collider.gameObject == gameObject)//"myGameObjectName")
			{
				//hit.GetComponent<TouchObjectScript>().ApplyForce();  
				SelectedOption();
			}
		}
	}
	
	void OnMouseDown()
	{
		SelectedOption();
	}
	
	void SelectedOption()
	{
		//Debug.Log("Selected this option " + gameObject.name);
		
		// Check if player didnt choose another thing before.
		// Thats cheating! XD
		if( !gearManagerScript.AreOptionsLocked() )
		{
			thisSelected = true;
			gearManagerScript.StartRotatingGears();
			gearManagerScript.LockOptions();
		}
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		// Only if an arrow collides with option stop everything
		if(collider.tag == "Arrow")
		{
			gearManagerScript.StopRotatingGears();
			// Check if the option selected by user is the correct one
			if( thisSelected )
			{
				gearManagerScript.WinGame();
			}
			else
			{
				gearManagerScript.LoseGame();
			}
		}
	}
}
